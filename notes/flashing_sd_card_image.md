Flashing the OS image onto the SD card
======================================

I am using a Mac, but the general idea is the same on linux: use dd

```

[ulrik@imac HomeAutomation]$ df -h
Filesystem      Size   Used  Avail Capacity   iused     ifree %iused  Mounted on
/dev/disk0s2   931Gi  396Gi  535Gi    43% 103843142 140137600   43%   /
devfs          213Ki  213Ki    0Bi   100%       738         0  100%   /dev
map -hosts       0Bi    0Bi    0Bi   100%         0         0  100%   /net
map auto_home    0Bi    0Bi    0Bi   100%         0         0  100%   /home
/dev/disk1s1    90Mi   25Mi   64Mi    29%       512         0  100%   /Volumes/NO NAME
[ulrik@imac HomeAutomation]$ 
[ulrik@imac HomeAutomation]$ 
[ulrik@imac HomeAutomation]$ 
[ulrik@imac HomeAutomation]$ diskutil unmount /dev/disk1s1
Volume NO NAME on disk1s1 unmounted
[ulrik@imac HomeAutomation]$ 
[ulrik@imac HomeAutomation]$ diskutil umount /dev/disk1s1 
Volume NO NAME on disk1s1 unmounted
[ulrik@imac HomeAutomation]$ 
[ulrik@imac HomeAutomation]$ 
[ulrik@imac HomeAutomation]$ sudo dd bs=1m if=archlinux_clean.img of=/dev/rdisk1
Password:
3839+0 records in
3839+0 records out
4025483264 bytes transferred in 422.689531 secs (9523499 bytes/sec)
[ulrik@imac HomeAutomation]$ 
```
