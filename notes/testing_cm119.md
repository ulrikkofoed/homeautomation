Testing the CM119 Electricity meter
===================================

According to the RFXtrx user guide (p.8) the OWL devices are supported by the "Oregon" 
protocol, which then need to be enabled on the RFXtrx device before it will output these
messages. 

[RFXtrx user guide](http://www.rfxcom.com/Documents/RFXtrx%20User%20Guide.pdf)

Below is a trace of how I enabled the Oregon protocol and started the listener. There are
two updates from the meter right at the bottom.

The python scripts are from the ["rfxcmd" package](https://code.google.com/p/rfxcmd)

```
[ulrik@imac rfxcmd]$ python2.7 rfxproto.py -h
Usage: rfxproto.py [options]

Options:
  -h, --help            show this help message and exit
  -d DEVICE, --device=DEVICE
                        Serial device of the RFXtrx433
  -l, --list            List all protocols
  -p PROTOCOL, --protocol=PROTOCOL
                        Protocol number
  -s STATE, --setstate=STATE
                        Set protocol state (on or off)
  -v, --save            Save current settings in device (Note device have
                        limited write cycles)
  -V, --version         Print rfxcmd version information
  -D, --debug           Debug logging on screen
[ulrik@imac rfxcmd]$ 
[ulrik@imac rfxcmd]$ 
[ulrik@imac rfxcmd]$ python2.7 rfxproto.py -d /dev/tty.usbserial-A1XPT51E -p 18 -s on --save
RFX protocols
-------------------------------------------------------------------
#	Protocol			State
-------------------------------------------------------------------
0	Undecoded			Disabled
1	RFU				Disabled
2	Byrox SX			Enabled
3	RSL				Disabled
4	Lightning4			Disabled
5	FineOffset/Viking		Disabled
6	Rubicson			Disabled
7	AE Blyss			Disabled
8	BlindsT1/T2/T3/T4		Disabled
9	BlindsT0			Disabled
10	ProGuard			Disabled
11	FS20				Disabled
12	La Crosse			Disabled
13	Hideki/UPM			Disabled
14	AD LightwaveRF			Enabled
15	Mertik				Disabled
16	BlindsT1/T2/T3/T4		Disabled
17	BlindsT0			Disabled
18	Oregon Scientific		Enabled
19	Meiantech			Disabled
20	HomeEasy EU			Disabled
21	AC				Disabled
22	ARC				Disabled
23	X10				Disabled
[ulrik@imac rfxcmd]$ 
[ulrik@imac rfxcmd]$ 
[ulrik@imac rfxcmd]$ python2.7 rfxproto.py -d /dev/tty.usbserial-A1XPT51E -l
RFX protocols
-------------------------------------------------------------------
#	Protocol			State
-------------------------------------------------------------------
0	Undecoded			Disabled
1	RFU				Disabled
2	Byrox SX			Enabled
3	RSL				Disabled
4	Lightning4			Disabled
5	FineOffset/Viking		Disabled
6	Rubicson			Disabled
7	AE Blyss			Disabled
8	BlindsT1/T2/T3/T4		Disabled
9	BlindsT0			Disabled
10	ProGuard			Disabled
11	FS20				Disabled
12	La Crosse			Disabled
13	Hideki/UPM			Disabled
14	AD LightwaveRF			Enabled
15	Mertik				Disabled
16	BlindsT1/T2/T3/T4		Disabled
17	BlindsT0			Disabled
18	Oregon Scientific		Enabled
19	Meiantech			Disabled
20	HomeEasy EU			Disabled
21	AC				Disabled
22	ARC				Disabled
23	X10				Disabled
[ulrik@imac rfxcmd]$ 
[ulrik@imac rfxcmd]$ 
[ulrik@imac rfxcmd]$ python2.7 rfxcmd.py -h
Usage: rfxcmd.py [options]

Options:
  -h, --help            show this help message and exit
  -d DEVICE, --device=DEVICE
                        The serial device of the RFXCOM, example /dev/ttyUSB0
  -l, --listen          Listen for messages from RFX device
  -x SIMULATE, --simulate=SIMULATE
                        Simulate one incoming data message
  -s SENDMSG, --sendmsg=SENDMSG
                        Send one message to RFX device
  -f, --rfxstatus       Get RFX device status
  -o CONFIG, --config=CONFIG
                        Specify the configuration file
  -v, --verbose         Output all messages to stdout
  -c, --csv             Output all messages to stdout in CSV format
  -V, --version         Print rfxcmd version information
  -D, --debug           Debug printout on stdout
  --listprotocol        List protocol settings
[ulrik@imac rfxcmd]$ 
[ulrik@imac rfxcmd]$ 
[ulrik@imac rfxcmd]$ python2.7 rfxcmd.py -d /dev/tty.usbserial-A1XPT51E  -v --listen
RFXCMD Version 0.3 ()




------------------------------------------------
Received		= 0D 01 00 00 02 53 E1 20 02 20 01 02 00 00
Date/Time		= 2014-09-19 20:11:08
Packet Length		= 0D
Packettype		= Interface Message
Subtype			= Interface response
Sequence nbr		= 00
Response on cmnd	= Get Status, return firmware versions and configuration of the interface.
Transceiver type	= 433.92MHz (Transceiver)
Firmware version	= 225
Protocols:
Undecoded                 Disabled
RFU                       Disabled
Byron SX                  Enabled
RSL                       Disabled
Lightning4                Disabled
FineOffset / Viking       Disabled
Rubicson                  Disabled
AE Blyss                  Disabled
Blinds T1/T2/T3/T4        Disabled
Blinds T0                 Disabled
ProGuard                  Disabled
FS20                      Disabled
La Crosse                 Disabled
Hideki / UPM              Disabled
AD Lightwave RF           Enabled
Mertik                    Disabled
Visonic                   Disabled
ATI                       Disabled
Oregon Scientific         Enabled
Meiantech                 Disabled
HomeEasy EU               Disabled
AC                        Disabled
ARC                       Disabled
X10                       Disabled





------------------------------------------------
Received		= 11 5A 01 00 F6 12 00 00 00 00 00 00 00 00 00 00 00 79
Date/Time		= 2014-09-19 20:11:43
Packet Length		= 11
Packettype		= Energy usage sensors
Subtype			= CM119/160
Seqnbr			= 00
Id			= F612
Count			= 0
Instant usage		= 0 Watt
Total usage		= 0 Wh
Battery			= 9
Signal level		= 7




------------------------------------------------
Received		= 11 5A 01 01 F6 12 00 00 00 00 00 00 00 00 00 00 00 79
Date/Time		= 2014-09-19 20:12:43
Packet Length		= 11
Packettype		= Energy usage sensors
Subtype			= CM119/160
Seqnbr			= 01
Id			= F612
Count			= 0
Instant usage		= 0 Watt
Total usage		= 0 Wh
Battery			= 9
Signal level		= 7



^C[ulrik@imac rfxcmd]$ 
```
