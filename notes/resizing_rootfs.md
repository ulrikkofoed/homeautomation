Resizing the rootfs on Arch Linux
=================================

Following instructions from here: http://raspberry-hosting.com/en/faq/cras-quis-nibh


```
[ulrik@imac ~]$ ssh root@domopi
root@domopi's password: 
Welcome to Arch Linux ARM

     Website: http://archlinuxarm.org
       Forum: http://archlinuxarm.org/forum
         IRC: #archlinux-arm on irc.Freenode.net

Last login: Fri Sep 12 23:26:41 2014 from imac.lan
[root@domopi ~]# 
[root@domopi ~]# 
[root@domopi ~]# 
[root@domopi ~]# df -h
Filesystem      Size  Used Avail Use% Mounted on
/dev/root       1.7G  719M  821M  47% /
devtmpfs         87M     0   87M   0% /dev
tmpfs            91M     0   91M   0% /dev/shm
tmpfs            91M  284K   91M   1% /run
tmpfs            91M     0   91M   0% /sys/fs/cgroup
tmpfs            91M     0   91M   0% /tmp
/dev/mmcblk0p1   90M   26M   65M  29% /boot
tmpfs            19M     0   19M   0% /run/user/0
[root@domopi ~]# 
[root@domopi ~]# 
[root@domopi ~]# ls /  
bin  boot  dev	etc  home  lib	lost+found  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
[root@domopi ~]# fdisk /dev/mmcblk0    

Welcome to fdisk (util-linux 2.25).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.


Command (m for help): p
Disk /dev/mmcblk0: 3.8 GiB, 4025483264 bytes, 7862272 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x417ee54b

Device         Boot  Start     End Sectors  Size Id Type
/dev/mmcblk0p1        2048  186367  184320   90M  c W95 FAT32 (LBA)
/dev/mmcblk0p2      186368 3667967 3481600  1.7G  5 Extended
/dev/mmcblk0p5      188416 3667967 3479552  1.7G 83 Linux

Command (m for help): d
Partition number (1,2,5, default 5): 2

Partition 2 has been deleted.

Command (m for help): n
Partition type
   p   primary (1 primary, 0 extended, 3 free)
   e   extended (container for logical partitions)
Select (default p): e
Partition number (2-4, default 2): 2
First sector (186368-7862271, default 186368): 
Last sector, +sectors or +size{K,M,G,T,P} (186368-7862271, default 7862271): 

Created a new partition 2 of type 'Extended' and of size 3.7 GiB.

Command (m for help): n
Partition type
   p   primary (1 primary, 1 extended, 2 free)
   l   logical (numbered from 5)
Select (default p): l

Adding logical partition 5
First sector (188416-7862271, default 188416): 
Last sector, +sectors or +size{K,M,G,T,P} (188416-7862271, default 7862271): 

Created a new partition 5 of type 'Linux' and of size 3.7 GiB.

Command (m for help): p
Disk /dev/mmcblk0: 3.8 GiB, 4025483264 bytes, 7862272 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x417ee54b

Device         Boot  Start     End Sectors  Size Id Type
/dev/mmcblk0p1        2048  186367  184320   90M  c W95 FAT32 (LBA)
/dev/mmcblk0p2      186368 7862271 7675904  3.7G  5 Extended
/dev/mmcblk0p5      188416 7862271 7673856  3.7G 83 Linux

Command (m for help): w
The partition table has been altered.
Calling ioctl() to re-read partition table.
Re-reading the partition table failed.: Device or resource busy

The kernel still uses the old table. The new table will be used at the next reboot or after you run partprobe(8) or kpartx(8).

[root@domopi ~]# reboot
Connection to domopi closed by remote host.
Connection to domopi closed.
[ulrik@imac ~]$ 
[ulrik@imac ~]$ 
[ulrik@imac ~]$ ssh root@domopi
root@domopi's password: 
Welcome to Arch Linux ARM

     Website: http://archlinuxarm.org
       Forum: http://archlinuxarm.org/forum
         IRC: #archlinux-arm on irc.Freenode.net

Last login: Sat Sep 13 20:16:29 2014 from imac.lan
[root@domopi ~]# 
[root@domopi ~]# 
[root@domopi ~]# resize2fs /dev/m
mapper/    md0        mem        mmcblk0    mmcblk0p1  mmcblk0p2  mmcblk0p5  mqueue/    
[root@domopi ~]# resize2fs /dev/mmcblk0p
mmcblk0p1  mmcblk0p2  mmcblk0p5  
[root@domopi ~]# resize2fs /dev/mmcblk0p5
resize2fs 1.42.11 (09-Jul-2014)
Filesystem at /dev/mmcblk0p5 is mounted on /; on-line resizing required
old_desc_blocks = 1, new_desc_blocks = 1
The filesystem on /dev/mmcblk0p5 is now 959232 blocks long.

[root@domopi ~]# 
[root@domopi ~]# df -h
Filesystem      Size  Used Avail Use% Mounted on
/dev/root       3.6G  720M  2.7G  21% /
devtmpfs         87M     0   87M   0% /dev
tmpfs            91M     0   91M   0% /dev/shm
tmpfs            91M  284K   91M   1% /run
tmpfs            91M     0   91M   0% /sys/fs/cgroup
tmpfs            91M     0   91M   0% /tmp
/dev/mmcblk0p1   90M   26M   65M  29% /boot
tmpfs            19M     0   19M   0% /run/user/0
[root@domopi ~]# 
```
