Updating the Raspberry PI firmware 
==================================

There is a firmware updater located here: https://github.com/Hexxeh/rpi-update

The rpi-update script however does have some dependencies which must be installed first.

The following trace contain the commands, complete with mistakes and all:

```
[pi@domopi ~]$ sudo pacman -S ca-certificates
[sudo] password for pi: 
Sorry, try again.
[sudo] password for pi: 
warning: ca-certificates-20140325-1 is up to date -- reinstalling
resolving dependencies...
looking for inter-conflicts...

Packages (1): ca-certificates-20140325-1

Total Download Size:    0.15 MiB
Total Installed Size:   0.32 MiB
Net Upgrade Size:       0.00 MiB

:: Proceed with installation? [Y/n] y
:: Retrieving packages ...
 ca-certificates-20140325-1-any                        157.9 KiB   321K/s 00:00 [##############################################] 100%
(1/1) checking keys in keyring                                                  [##############################################] 100%
(1/1) checking package integrity                                                [##############################################] 100%
(1/1) loading package files                                                     [##############################################] 100%
(1/1) checking for file conflicts                                               [##############################################] 100%
(1/1) checking available disk space                                             [##############################################] 100%
(1/1) reinstalling ca-certificates                                              [##############################################] 100%
synchronizing filesystem...
[pi@domopi ~]$ 

[pi@domopi ~]$ sudo pacman -S git
resolving dependencies...
looking for inter-conflicts...

Packages (2): perl-error-0.17022-1  git-2.1.0-1

Total Download Size:    3.43 MiB
Total Installed Size:   20.21 MiB

:: Proceed with installation? [Y/n] y
:: Retrieving packages ...
 perl-error-0.17022-1-any                               18.3 KiB   158K/s 00:00 [##############################################] 100%
 git-2.1.0-1-armv6h                                      3.4 MiB   378K/s 00:09 [##############################################] 100%
(2/2) checking keys in keyring                                                  [##############################################] 100%
(2/2) checking package integrity                                                [##############################################] 100%
(2/2) loading package files                                                     [##############################################] 100%
(2/2) checking for file conflicts                                               [##############################################] 100%
(2/2) checking available disk space                                             [##############################################] 100%
(1/2) installing perl-error                                                     [##############################################] 100%
(2/2) installing git                                                            [##############################################] 100%
Optional dependencies for git
    tk: gitk and git gui
    perl-libwww: git svn
    perl-term-readkey: git svn
    perl-mime-tools: git send-email
    perl-net-smtp-ssl: git send-email TLS support
    perl-authen-sasl: git send-email TLS support
    python2: various helper scripts [installed]
    subversion: git svn
    cvsps2: git cvsimport
    gnome-keyring: GNOME keyring credential helper
synchronizing filesystem...
[pi@domopi ~]$ sudo curl -L --output /usr/bin/rpi-update https://raw.githubusercontent.com/Hexxeh/rpi-update/master/rpi-update && sudo chmod +x /usr/bin/rpi-update^C
[pi@domopi ~]$ which curl
/usr/bin/curl
[pi@domopi ~]$ sudo curl -L --output /usr/bin/rpi-update https://raw.githubusercontent.com/Hexxeh/rpi-update/master/rpi-update && sudo chmod +x /usr/bin/rpi-update
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7100  100  7100    0     0  13028      0 --:--:-- --:--:-- --:--:-- 13295
[pi@domopi ~]$ ll /usr/bin/rpi-update 
-bash: ll: command not found
[pi@domopi ~]$ ls -l /usr/bin/rpi-update 
-rwxr-xr-x 1 root root 7100 Sep 13 21:34 /usr/bin/rpi-update
[pi@domopi ~]$ sudo rpi-update 
 *** Raspberry Pi firmware updater by Hexxeh, enhanced by AndrewS and Dom
 *** Performing self-update
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7100  100  7100    0     0  15231      0 --:--:-- --:--:-- --:--:-- 15604
 *** Relaunching after update
 *** Raspberry Pi firmware updater by Hexxeh, enhanced by AndrewS and Dom
 !!! This tool requires you have readelf installed, please install it first
     In Debian, try: sudo apt-get install binutils
     In Arch, try: pacman -S binutils
[pi@domopi ~]$ sudo pacman -S binutils gcc make
resolving dependencies...
looking for inter-conflicts...

Packages (10): cloog-0.18.1-2  gc-7.4.2-2.1  guile-2.0.11-1  isl-0.12.2-1  libatomic_ops-7.4.2-1  libltdl-2.4.2-12  libmpc-1.0.2-2
               binutils-2.24-1  gcc-4.8.2-7  make-4.0-2

Total Download Size:    24.10 MiB
Total Installed Size:   99.40 MiB

:: Proceed with installation? [Y/n] y
:: Retrieving packages ...
 binutils-2.24-1-armv6h                                  3.6 MiB   384K/s 00:10 [##############################################] 100%
 libmpc-1.0.2-2-armv6h                                  52.2 KiB   242K/s 00:00 [##############################################] 100%
 isl-0.12.2-1-armv6h                                   324.2 KiB   353K/s 00:01 [##############################################] 100%
 cloog-0.18.1-2-armv6h                                  56.1 KiB   248K/s 00:00 [##############################################] 100%
 gcc-4.8.2-7-armv6h                                     16.4 MiB   384K/s 00:44 [##############################################] 100%
 libltdl-2.4.2-12-armv6h                               143.3 KiB   317K/s 00:00 [##############################################] 100%
 make-4.0-2-armv6h                                     360.0 KiB   356K/s 00:01 [##############################################] 100%
 libatomic_ops-7.4.2-1-armv6h                           57.1 KiB   250K/s 00:00 [##############################################] 100%
 gc-7.4.2-2.1-armv6h                                   194.2 KiB   332K/s 00:01 [##############################################] 100%
 guile-2.0.11-1-armv6h                                   2.9 MiB   383K/s 00:08 [##############################################] 100%
(10/10) checking keys in keyring                                                [##############################################] 100%
(10/10) checking package integrity                                              [##############################################] 100%
(10/10) loading package files                                                   [##############################################] 100%
(10/10) checking for file conflicts                                             [##############################################] 100%
(10/10) checking available disk space                                           [##############################################] 100%
( 1/10) installing binutils                                                     [##############################################] 100%
( 2/10) installing libmpc                                                       [##############################################] 100%
( 3/10) installing isl                                                          [##############################################] 100%
( 4/10) installing cloog                                                        [##############################################] 100%
( 5/10) installing gcc                                                          [##############################################] 100%
( 6/10) installing libltdl                                                      [##############################################] 100%
( 7/10) installing libatomic_ops                                                [##############################################] 100%
( 8/10) installing gc                                                           [##############################################] 100%
( 9/10) installing guile                                                        [##############################################] 100%
(10/10) installing make                                                         [##############################################] 100%
synchronizing filesystem...
[pi@domopi ~]$ ntp
ntp-keygen  ntp-wait    ntpd        ntpdate     ntpdc       ntpq        ntptime     ntptrace    
[pi@domopi ~]$ man ntpdate
No manual entry for ntpdate
[pi@domopi ~]$ ntpdate -h
ntpdate: unknown option -h
usage: ntpdate [-46bBdqsuv] [-a key#] [-e delay] [-k file] [-p samples] [-o version#] [-t timeo] server ...
[pi@domopi ~]$ ping uk.pool.ntp.org
ping: icmp open socket: Operation not permitted
[pi@domopi ~]$ ping 0.uk.pool.ntp.org
ping: icmp open socket: Operation not permitted
[pi@domopi ~]$ date
Sat Sep 13 21:41:40 BST 2014
[pi@domopi ~]$ cat /etc/ntp.conf
# With the default settings below, ntpd will only synchronize your clock.
#
# For details, see:
# - the ntp.conf man page
# - http://support.ntp.org/bin/view/Support/GettingStarted
# - https://wiki.archlinux.org/index.php/Network_Time_Protocol_daemon

# Associate to public NTP pool servers; see http://www.pool.ntp.org/
server 0.pool.ntp.org iburst
server 1.pool.ntp.org iburst
server 2.pool.ntp.org iburst

# Only allow read-only access from localhost
restrict default noquery nopeer
restrict 127.0.0.1
restrict ::1

# Location of drift file
driftfile /var/lib/ntp/ntp.drift
[pi@domopi ~]$ 
[pi@domopi ~]$ 
[pi@domopi ~]$ sudo rpi-update 
[sudo] password for pi: 
 *** Raspberry Pi firmware updater by Hexxeh, enhanced by AndrewS and Dom
 *** Performing self-update
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7100  100  7100    0     0  12610      0 --:--:-- --:--:-- --:--:-- 12839
 *** Relaunching after update
 *** Raspberry Pi firmware updater by Hexxeh, enhanced by AndrewS and Dom
 *** We're running for the first time
 *** Backing up files (this will take a few minutes)
 *** Backing up firmware
 *** Backing up modules 3.12.28-1-ARCH
 *** Downloading specific firmware revision (this will take a few minutes)
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   168    0   168    0     0    208      0 --:--:-- --:--:-- --:--:--   211
100 21.3M  100 21.3M    0     0   361k      0  0:01:00  0:01:00 --:--:--  450k
 *** Updating firmware
 *** Updating kernel modules
 *** depmod 3.12.28+
 *** Updating VideoCore libraries
 *** Using HardFP libraries
 *** Updating SDK
 *** Running ldconfig
 *** Storing current firmware revision
 *** Deleting downloaded files
 *** Syncing changes to disk
 *** If no errors appeared, your firmware was successfully updated to a71a98c35c37c7e8147c8c6d3bdc2622843185dd
 *** A reboot is needed to activate the new firmware
[pi@domopi ~]$ 
[pi@domopi ~]$ 
[pi@domopi ~]$ sudo shutdown -r now
Connection to domopi closed by remote host.
Connection to domopi closed.
```
