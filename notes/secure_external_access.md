Enabling secure external access
===============================

The web-interface is not secure in itself. It has some authentication features, but they
are not particularly safe and seem to be a bit difficult to get to work: in my first
attempts they failed unsafe - i.e. the security was enabled, but allowed access straight
through without any attempts to authenticate! Of course I may have been doing something 
wrong, but I was just going through the settings page.

Instead my chosen solution: PPTP VPN running on the rPI - also using iptables.

Installation is very simple:

    pacman -S pptpd

Configuration is a bit more difficult, but is well explained here: 
https://wiki.archlinux.org/index.php/PPTP_Server 

A simpler, but less complete version is 
here: http://www.domoticz.com/wiki/Installing_a_PPTP-VPN_server_on_a_Raspberry_Pi

Note on DNS: if you want to be able to use your local (lan) name for your rPI as the
URL to connect to, set the ms-dns <your router IP> at the end of /etc/ppp/options.pptpd
and comment out any other ms-dns entries.

This works well with my iphone at least.

