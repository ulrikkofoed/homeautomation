Enabling a swab file
====================

By default the Arch Linux has no swap disk - and with very little RAM it is brittle.

My experience with the raspberry pi 256MB RAM is too little to build domoticz. One of the
very first compilation steps fall over with a virtual memory allocation error. With a swap
file of 512MB there is enough to build domoticz albeit slowly...

Enabling a swap file in the following. Mostly based on instructions from here: 
http://www.raspberrypi.org/forums/viewtopic.php?f=28&t=7839

```
[root@domopi ~]# dd if=/dev/zero of=/swap bs=1M count=512
512+0 records in
512+0 records out
536870912 bytes (537 MB) copied, 52.7028 s, 10.2 MB/s
[root@domopi ~]# chmod 0600 /swap 
[root@domopi ~]# mkswap /swap 
Setting up swapspace version 1, size = 524284 KiB
no label, UUID=981efb1a-1144-485e-b932-2678b9c1492b
[root@domopi ~]# echo "/swap swap swap" >> /etc/fstab
[root@domopi ~]# swapon -a
[root@domopi ~]# 
[root@domopi ~]# me 
mesg  mev   
[root@domopi ~]# top

top - 20:55:35 up 18 min,  1 user,  load average: 0.39, 0.34, 0.17
Tasks:  64 total,   1 running,  63 sleeping,   0 stopped,   0 zombie
%Cpu(s):  1.0 us,  2.5 sy,  0.0 ni, 92.9 id,  3.4 wa,  0.1 hi,  0.0 si,  0.0 st
KiB Mem:    185312 total,   134664 used,    50648 free,     3584 buffers
KiB Swap:   524284 total,        0 used,   524284 free.   107320 cached Mem

  PID USER      PR  NI    VIRT    RES    SHR S %CPU %MEM     TIME+ COMMAND                                                          
  289 root      20   0    2952   1168    840 R 11.1  0.6   0:00.06 top                                                              
    1 root      20   0    5000   3364   2412 S  0.0  1.8   0:06.40 systemd                                                          
    2 root      20   0       0      0      0 S  0.0  0.0   0:00.00 kthreadd                                                         
    3 root      20   0       0      0      0 S  0.0  0.0   0:00.17 ksoftirqd/0                                                      
...
... <lots of top output cut away> ....
...
                                                   
[root@domopi ~]# 
[root@domopi ~]# cat /proc/swaps  
Filename				Type		Size	Used	Priority
/swap                                   file		524284	0	-1
[root@domopi ~]# cat /proc/meminfo  
MemTotal:         185312 kB
MemFree:           50808 kB
Buffers:            3608 kB
Cached:           107332 kB
SwapCached:            0 kB
Active:            25216 kB
Inactive:          94744 kB
Active(anon):       9164 kB
Inactive(anon):      144 kB
Active(file):      16052 kB
Inactive(file):    94600 kB
Unevictable:           0 kB
Mlocked:               0 kB
SwapTotal:        524284 kB
SwapFree:         524284 kB
Dirty:                 8 kB
Writeback:             0 kB
AnonPages:          9036 kB
Mapped:             8368 kB
Shmem:               288 kB
Slab:               7340 kB
SReclaimable:       4328 kB
SUnreclaim:         3012 kB
KernelStack:         512 kB
PageTables:          448 kB
NFS_Unstable:          0 kB
Bounce:                0 kB
WritebackTmp:          0 kB
CommitLimit:      616940 kB
Committed_AS:      21304 kB
VmallocTotal:     827392 kB
VmallocUsed:        1016 kB
VmallocChunk:     608740 kB
[root@domopi ~]# 
```
