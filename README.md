Home Automation Project
=======================

This contains my notes and configurations of my personal home automation project. The details may not be of interest to many others than myself, but feel free to browse through it all...

Notes for installation
----------------------

Preparation of the Raspberry PI and the build of Domoticz.

* [Creating a swap file](notes/creating_swap_file.md)
* [Resizing the rootfs](notes/resizing_rootfs.md)
* [Flashing the SD card image](notes/flashing_sd_card_image.md)
* [Updating the raspberry pi firmware](notes/updating_raspberry_firmware.md)

Just a little change.
